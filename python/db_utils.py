"""
Author: Miguel Agueda-Cabral
Project: Software Engineering Term Project, Baker's Ear.
         Available at https://gitlab.com/MiguelAgueda/cs4800-project
File: Python/db_utils.py
Date: 27 November 2020
Description:
    Database Utilities Class
    Implements base tools for communicating with a MongoDB cloud service.
"""

from bson.objectid import ObjectId
from datetime import datetime
import os
from pymongo import MongoClient



class BaseDBTools(object):
    """ Handles connecting to a local or remote database.

    Attributes
    ----------
        local : Boolean
            Controls the use of a local/cloud database.
            Set to 'False' to connect to cloud database. Defaults to local database.
    """

    def __init__(self):
        self.local = True
        self.client = MongoClient("127.0.0.1")
        self.__env_var_db_user = None
        self.__env_var_db_pass = None

    @property
    def local(self):
        """Return object's `local` variable."""
        return self.local

    @local.setter
    def local(self, local):
        """ Controls the connection to database on MongoDB cloud.

        Assumes that username and password are stored as environment variables
        `env_var_db_user` and `env_var_db_pass`, respectively.
        This avoids the exposure of secret keys via push to GitHub.
        """
        if not local:
            # Get DB username from environment var.
            # db_user = os.environ.get('CS4800_DB_USER')
            db_user = os.environ.get(self.__env_var_db_user, "")
            # Get DB password from environment var.
            # db_pass = os.environ.get('CS4800_DB_PASS')
            db_pass = os.environ.get(self.__env_var_db_pass, "")

            assert len(db_user) > 0, F"{self.__env_var_db_user} not defined in environment."
            assert len(db_pass) > 0, F"{self.__env_var_db_pass} not defined in environment."

            # Create connection string using user/pass.
            conn_str = F'mongodb+srv://{db_user}:{db_pass}@cluster0.8c8sg.mongodb.net/test?retryWrites=true&w=majority'
            self.client = MongoClient(
                conn_str, connectTimeoutMS=5000, connect=True)

    def set_env_vars(self, user_env_var, pass_env_var):
        """ Set environment variable names for database credentials.

        Parameters
        ----------
            user_env_var: String containing database username environment variable name.
            pass_env_var: String containing database password environment variable name.
        """

        self.__env_var_db_user = user_env_var
        self.__env_var_db_pass = pass_env_var


class DBTools(BaseDBTools):
    """ This module provides CRUD operators for a database. 

    Note
    ----
        To connect to live database, set instance's 'local' attribute to False.
        `(UserDBTools Instance).local = False`

    Functions
    ---------
        create: Create a new order.

        read: Read in all orders.

        update: Update an existing order.

        delete: Delete an existing order.
    """

    def __init__(self):
        super().__init__()

    def create(self, order_form):
        """ Add new order to 'orders' collection in database.

        Parameters
        ----------
            order_form: Dictionary
                Dictionary containing order information.

        Returns
        -------
            True: Order created successfully.

            False: Order not created successfully.
        """

        # order_form["date"] = datetime.utcnow()
        result = self.client.db.orders.insert_one(order_form)
        print(F"db.create: {result.inserted_id}")
         
        return True, result.inserted_id

    def read_all(self):
        """ Read all orders from database.

        Returns
        -------
            orders: List of orders.
        """

        orders = {}  # Create container for all orders.
        # Get all orders.
        for i, order in enumerate(list(self.client.db.orders.find({}))):
            order["_id"] = str(order["_id"])
            orders[i] = order

        return orders

    def read(self, _id):
        """ Extract specific order from database.

        Parameters
        ----------
            _id: ObjectID of order to return.

        Returns
        -------
            order: Dictionary containing order details.
        """

        order = self.client.db.orders.find_one(
            {"_id": ObjectId(_id)})

        order["_id"] = str(order["_id"])

        return order

    def update(self, _id, updated_order):
        """ Update details of an existing order.

        Parameters
        ----------
            id: ObjectID of order to update.

        Returns
        -------
            True: Update was successful.
            False: Update was not successful.
        """

        result = self.client.db.orders.update_one({"_id": ObjectId(_id)},
                                                  {"$set": updated_order})
        if result.matched_count == 1:
            return True
        else:
            return False
    
    def get_inventory(self):
        """ Get existing inventory from database.

        Parameters
        ----------
            None.
        
        Returns
        -------
            inventory: Dictionary of current inventory.
        """

        inventory = list(self.client.db.inventory.find({}))
        if len(inventory) == 1:
            inventory = inventory[0]
        
        else:
            inventory = {}
        
        return inventory

    def decrement_inventory(self, order):
        """ Decrement inventory quantities based on customer order."""

        status = False
        current_inventory = self.get_inventory()
        new_inventory = {}
        for item in current_inventory:
            for ordered_item in order["order"]:
                try:
                    if ordered_item["cookie"] == item:
                        print(F"Matched: {item}")
                        new_inventory[item] = current_inventory[item] - int(ordered_item["quantity"])
                    else:
                        pass
                        
                except:
                    pass

            if len(new_inventory) > 0:
                status = True
                self.update_inventory(new_inventory)
        
        return status
    
    def update_inventory(self, updated_inventory):
        """ Update existing inventory with overwriting update.
        
        Parameters
        ----------
            updated_inventory: Dictionary with inventory. 

        Returns
        -------
            True: Update was successful.
            False: Update was not successful.
        """

        try:
            inventory = list(self.client.db.inventory.find({}))[0]

        except IndexError:
            self.client.db.inventory.insert_one({})
            inventory = list(self.client.db.inventory.find({}))[0]

        result = self.client.db.inventory.update_one({"_id": inventory["_id"]},
                                                     {"$set": updated_inventory})

        if result.matched_count == 1:
            return True
        else:
            return False

    def delete(self, id):
        """ Delete an existing order.

        Parameters
        ----------
            id: ObjectID of order to delete.

        Returns
        -------
            True: Deletion was successful.
            False: Deletion was not successful.
        """

        result = self.client.db.orders.delete_one({"_id": id})
        if result.deleted_count == 0:
            return False

        return True
