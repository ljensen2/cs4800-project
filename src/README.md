# El Nene's Cookies Backend Management Program

## About
This project is designed to be used by El Nene's Cookies workers as an order management system. The program connects to the El Nene's API hosted on Heroku, and supports fulfilling orders and inventory tracking.

### Unfulfilled Orders Screen
![Unfulfilled Orders](https://i.imgur.com/7z6DS8d.png "Unfulfilled Orders")

### Past Orders Screen
![Past Orders](https://i.imgur.com/Jr3NmWG.png "Past Orders")

### Inventory Screen
![Inventory](https://i.imgur.com/v9tCSqM.png "Inventory")


## Author of Folder
* Liam Jensen (besides json and cURLpp libraries)

## Open Source Projects Used
* [cURLpp](https://github.com/jpbarrette/curlpp)
* [nlohmann json](https://github.com/nlohmann/json)
* [Qt5 Tools](https://www.qt.io/product/development-tools)

## Dependencies
* qt5 (qmake)
* \>=C++17 compiler

## Running
* Install Qt5 dependencies (see **Note** below for more information)
* Run `qmake` within the `cs4800-project/src` directory.
* Run `cookiesales` from the `bin` directory.
* The order screens will be populated based on the El Nene's Cookies API. New orders will appear as soon as they are placed.
* The Unfulfilled Orders Screen, Past Orders Screen, and Inventory Screen can be selected from the buttons on the left. Clicking "Done" on an order marks the order as Fulfilled and ready to be picked up, or clicking "Cancel" will mark the order as cancelled. Cancelled orders appear in red in the Past Orders Screen, while Fulfilled orders appear in blue.
---
* **Note:** QtCreator is the IDE designed to create Qt applications. Installing QtCreator will likely install necessary dependencies, and will allow you to build the application within the IDE itself. Since C++ is compiled and very platform-specific, I recommend installing the QtCreator IDE to ensure that the dependencies are installed correctly. I have researched using Docker for cross-compilation, but could not find an effective solution to build many different release types on one machine. Please send an email to my school email or create an issue on gitlab if there are issues.

## File Descriptions
* `cookiesales.pro`: Qt project file for QtCreator IDE
* `*.qml`: Defines UI views. `main.qml` is the entrypoint, and other `.qml` files are used to switch to different screens (Unfulfilled Orders, Past Orders, etc.)
* `main.cpp`: Application entrypoint. Starts Qt UI.
* `api.*pp` and `apiQt.*pp`: Static classes used to connect to the El Nene's Cookies backend ordering API. `apiQt` wraps the `api` functions in a way that can be called from within `qml`.
* `order.*pp`: Class to model a standard order from the API. Holds customer information (name, email) and order details (SalesItems).
* `salesitem.*pp`: Class to model an item being sold on El Nene's Cookies website. Holds item name (Plain cookie, Chocolate Chip cookie) and amount of units sold. `order.*pp` maintains a list of SalesItem
* `json.hpp` and `cURLpp` directory: Open source projects used for HttpRequests and JSON parsing from within C++. Both are used to communicate with the order API.