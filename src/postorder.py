
import requests
import json

URL = "https://testingbakersear.herokuapp.com/api/submit_order"

name = input("Name: ")
email = input("Email: ")

cookies = []

plain_num = int(input("Enter amount of plain cookies: "))
choc_chip_num = int(input("Enter amount of choc chip cookies: "))
mango_num = int(input("Enter amount of mango cranberry cookies: "))

if(plain_num>0):
    cookies.append({"cookie":"Plain", "quantity":plain_num})
if(choc_chip_num>0):
    cookies.append({"cookie":"Chocolate chip", "quantity":choc_chip_num})
if(mango_num>0):
    cookies.append({"cookie":"Mango Cranberry", "quantity":mango_num})

order = {
    "name" : name,
    "email" : email,
    "order" : cookies
}

print(json.dumps(order))
req = requests.post(URL, data=json.dumps(order))
print(req.content)

