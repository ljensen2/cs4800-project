/**
  Author: Liam Jensen
*/

#ifndef API_CPP
#define API_CPP

#include <string>
#include "api.hpp"

// Get all orders from DB in QList (for views)
// TODO: revise order specification, get orders depending on order type (unfulfilled, completed, etc.)
/**
 * @brief API::getOrders
 * @return QList of Order object
 * Returns all orders from the Baker's Ear API
 */
#define PRICE 5 // TODO: Remove (get from API?)

QList<QObject*> API::getUnfulfilledOrders() {
    QList<QObject*> orders;

    nlohmann::json j = getOrdersJson();

    // Populate orders
    for(auto& element : j) {
        QList<SalesItem*> items;
        // Populate orders from json
        try {
            if(element["status"] == "unfulfilled")
                orders.append(API::fromJsonElement(element));
        } catch(nlohmann::json::type_error &e) {
            std::cerr << "Could not parse element " << element << " to order" << std::endl;
            std::cerr << e.what() << std::endl;
            std::flush(std::cerr);
        }
    }

    return orders;
}

QList<QObject*> API::getPastOrders() {
    QList<QObject*> orders;

    nlohmann::json j = getOrdersJson();

    // Populate orders
    for(auto& element : j) {
        QList<SalesItem*> items;
        // Populate orders from json
        try {
            if(element["status"] != "unfulfilled")
                orders.append(API::fromJsonElement(element));
        } catch(nlohmann::json::type_error &e) {
            std::cerr << "Could not parse element " << element << " to order" << std::endl;
            std::cerr << e.what() << std::endl;
            std::flush(std::cerr);
        }
    }

    return orders;
}

// Helper function to create update_order dict objects for API
// {_id : id, "order" : {"status" : status}}
nlohmann::json orderStatusJSON(std::string orderID, std::string orderStatus) {
    nlohmann::json status;
    status["_id"] = orderID;
    nlohmann::json orderJSON;
    orderJSON["status"] = orderStatus;
    status["order"] = orderJSON;
    return status;
}

// Sets the Order at orderID to "fulfilled"
bool API::setOrderFulfilled(std::string orderID) {
    nlohmann::json body = orderStatusJSON(orderID, "fulfilled");
    return API::performPOSTRequest(API::updateOrderURL, body);
}

// Sets the Order at orderID to "cancelled"
bool API::setOrderCancelled(std::string orderID) {
    nlohmann::json body = orderStatusJSON(orderID, "cancelled");
    return API::performPOSTRequest(API::updateOrderURL, body);
}

// Throws nlohmann::json::type_error
Order* API::fromJsonElement(nlohmann::json element) {
    auto& orderJSON = element["order"];
    QList<SalesItem*> items;

    for(auto& orderDetails : orderJSON.items()) {
        int quantity = orderDetails.value()["quantity"];
        double price = PRICE * quantity;
        items.append(new SalesItem(API::fromStr(orderDetails.value()["cookie"]), quantity, price));
    }
    return new Order(API::fromStr(element["_id"]), API::fromStr(element["name"]), API::fromStr(element["email"]), API::fromStr("0-0-0-0"), API::fromStr(element["status"]), items);
}


// Get inventory from DB in QList (for views)
/**
 * @brief API::getInventory
 * @return QList of SalesItem object (with quantity = inventory quantity, price = 0)
 * Returns inventory from the Baker's Ear API
 */
QList<QObject*> API::getInventory() {
    QList<QObject*> inventory;

    nlohmann::json j = getInventoryJson();

    try {
        for(auto& element : j.items()) {
            int count = (int)element.value();
            // SalesItem holds information for Inventory item (with 0 as price)
            SalesItem* item = new SalesItem(API::fromStr(element.key()), count, 0);
            inventory.append(item);
        }
    } catch(nlohmann::json::type_error &e) {
        std::cerr << "Could not parse element to inventory" << std::endl;
        std::cerr << e.what() << std::endl;
        std::flush(std::cerr);
    }

    return inventory;
}

#endif // API_CPP
