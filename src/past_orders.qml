/**
  Author: Liam Jensen
*/

import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Item {
    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        Text {
            y: 0
            text: "Past Orders (" + pastOrders.length + ")"
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillHeight: false
            Layout.fillWidth: true
            font.underline: true
            anchors.leftMargin: 5
            anchors.topMargin: 15
            font.pixelSize: 24
        }

        ListView {
            x: 0
            y: 0
            spacing: 5
            Layout.margins: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.topMargin: 0
            layer.textureMirroring: ShaderEffectSource.MirrorVertically
            clip: true
            cacheBuffer: 320
            pixelAligned: false
            boundsBehavior: Flickable.StopAtBounds
            enabled: true
            ScrollBar.vertical: ScrollBar {
                id: scroll
                policy: ScrollBar.AlwaysOn
            }

            model: pastOrders

            delegate: Pane { // All orders will populate the delegate
                anchors.left: parent.left
                // Scrollbar goes over view otherwise
                width: parent.width - scroll.width
                contentHeight: 50
                topPadding: 0
                rightPadding: 0
                leftPadding: 0
                bottomPadding: 0

                Rectangle { // Background
                    color: orderStatus == "cancelled" ? "#ff8f8f" : "#f7f7ff"
                    anchors.fill: parent
                }

                RowLayout { // Text summary layout
                    anchors.fill: parent
                    spacing: 5
                    RowLayout {
                        Layout.alignment: Qt.AlignLeft
                        Layout.fillWidth: true
                        Text {
                            y: 0
                            font.pixelSize: 20
                            text: customerName
                            Layout.fillWidth: false
                            Layout.alignment: Qt.AlignLeft
                            anchors.leftMargin: 0
                        }

                        Text {
                            text: orderSummary
                            wrapMode: Text.WordWrap
                            Layout.fillHeight: false
                            Layout.fillWidth: true
                            font.pixelSize: 12
                        }

                        Text {
                            x: 0
                            y: 0
                            font.pixelSize: 20
                            text: "$" + totalPrice.toFixed(2)
                            Layout.alignment: Qt.AlignRight
                            anchors.rightMargin: 0
                        }

                    }
                }
            }
        }
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:3;anchors_height:100;anchors_width:100}
D{i:1;anchors_height:172;anchors_width:279;anchors_x:0;anchors_y:224}
}
##^##*/
