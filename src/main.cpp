/**
  Author: Liam Jensen
*/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
// include qml context, required to add a context property
#include <QQmlContext>
#include <QList>

#include <vector>
#include <iostream>
#include <fstream>

#include "order.hpp"
#include "salesitem.hpp"
#include "json.hpp"
#include "apiQt.hpp"

// App entry point
int main(int argc, char *argv[])
{
    // Initialize Qt backend
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // Load main QML (view)
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    APIQtWrapper* qAPI = new APIQtWrapper(&engine);

    // Give API to Qt
    engine.rootContext()->setContextProperty("api", qAPI);
    engine.load(url);

    return app.exec();
}
