//Thomas Lewis, Baker's Ear Group
//CS4800- El Nene's Wonder Cookies website

let carts = document.querySelectorAll('.add-cart');


let products = [
  {
    order: 'ChocolateChip',
    tag:'chocolatechip',
    price: 5,
    quantity: 0
  },

  {
    order: 'Mangocranberry',
    tag:'mangocranberry',
    price: 5,
    quantity: 0
  },

  {
    order: 'Plain',
    tag:'plain',
    price: 5,
    quantity: 0
  }
]
for (let i=0; i < carts.length; i++) {
    carts[i].addEventListener('click', () => {
      cartNumbers(products[i]);
      totalCost(products[i]);
      console.log(products[2].order);
    })
}

function onLoadCartNumbers() {
  let productNumbers = localStorage.getItem('cartNumbers');
  if (productNumbers) {
    document.querySelector('.cart span').textContent = productNumbers;
  }
}


function cartNumbers(product, action) {
  let productNumbers = localStorage.getItem('cartNumbers');
  productNumbers = parseInt(productNumbers);

  let cartItems = localStorage.getItem('productsquantity');
  cartItems = JSON.parse(cartItems);

  if (action == "decrease") {
    localStorage.setItem('cartNumbers', productNumbers -1);
    document.querySelector('.cart span').textContent = productNumbers - 1;
  } else if(productNumbers) {
    localStorage.setItem("cartNumbers", productNumbers + 1);
    document.querySelector('.cart span').textContent = productNumbers + 1;
  } else {
    localStorage.setItem('cartNumbers', 1);
    document.querySelector('.cart span').textContent = 1;
  }
  setItems(product);
}
//Stores Product information in browser local storage
function setItems(product) {
  let cartItems = localStorage.getItem('productsquantity');
  cartItems = JSON.parse(cartItems);
  //Check if cart is empty for first time use, increasing by 1 each click
  if(cartItems != null) {

    if(cartItems[product.tag] == undefined) {
      cartItems = {
        ...cartItems,
        [product.tag]: product
      }
    }
    cartItems[product.tag].quantity += 1;
  } else {
    product.quantity = 1;
    cartItems = {
      [product.tag]: product
  }
}
  localStorage.setItem("productsquantity", JSON.stringify(cartItems));
}

//Calculates the total cost of cart
function totalCost(product, action) {
  let cartCost = localStorage.getItem('totalCost');

  if (action =="decrease"){
    cartCost = parseInt(cartCost);
    localStorage.setItem('totalCost', cartCost - product.price);
  }else if (cartCost !=null){
    cartCost = parseInt(cartCost); //converts cost string to cost int
    localStorage.setItem("totalCost", cartCost + product.price);
  } else {
  localStorage.setItem("totalCost", product.price);
  }
}

function deleteButtons() {
  let deleteButtons = document.querySelectorAll('.products ion-icon');
  let productName;
  let productNumbers = localStorage.getItem('cartNumbers');
  let cartItems = localStorage.getItem('productsquantity');
  cartItems = JSON.parse(cartItems);
  let cartCost = localStorage.getItem('totalCost');

  for(let i=0; i < deleteButtons.length; i++) {
    deleteButtons[i].addEventListener('click', () => {
      productName = deleteButtons[i].parentElement.textContent.trim().toLowerCase().replace(/ /g, '');
        localStorage.setItem('cartNumbers', productNumbers - cartItems[productName].quantity);
        localStorage.setItem('totalCost', cartCost - (cartItems[productName].price * cartItems[productName].quantity));

        delete cartItems[productName];
        localStorage.setItem('productsquantity', JSON.stringify(cartItems));

        displayCart();
        onLoadCartNumbers();
    });
  }
}


function manageQuantity() {
  let decreaseButtons = document.querySelectorAll('.decrease');
  let increaseButtons = document.querySelectorAll('.increase');
  let cartItems = localStorage.getItem('productsquantity');
  let currentQuantity = 0;
  let currentProduct = "";
  cartItems = JSON.parse(cartItems);

  for(let i=0; i < decreaseButtons.length; i++) {
    decreaseButtons[i].addEventListener('click', () => {
        currentQuantity = decreaseButtons[i].parentElement.querySelector('span').textContent;
        currentProduct = decreaseButtons[i].parentElement.previousElementSibling.previousElementSibling.querySelector('span').textContent.toLowerCase();

      if (cartItems[currentProduct].quantity > 1) {
          cartItems[currentProduct].quantity -= 1;
          cartNumbers(cartItems[currentProduct], "decrease");
          totalCost(cartItems[currentProduct], "decrease");
          localStorage.setItem('productsquantity', JSON.stringify(cartItems));
          displayCart();
      }
    });
    }

  for(let i=0; i < increaseButtons.length; i++) {
    increaseButtons[i].addEventListener('click', () => {
      currentQuantity = increaseButtons[i].parentElement.querySelector('span').textContent;
      currentProduct = increaseButtons[i].parentElement.previousElementSibling.previousElementSibling.querySelector('span').textContent.toLowerCase();

        cartItems[currentProduct].quantity += 1;
        cartNumbers(cartItems[currentProduct]);
        totalCost(cartItems[currentProduct]);
        localStorage.setItem('productsquantity', JSON.stringify(cartItems));
        displayCart();
      })
  }
}

function displayCart() {
  let cartItems = localStorage.getItem("productsquantity");
  cartItems = JSON.parse(cartItems);
  let productContainer = document.querySelector(".products");
  let cartCost = localStorage.getItem('totalCost');
  if(cartItems && productContainer) {
    productContainer.innerHTML = ''; //Keeps initial page empty
    Object.values(cartItems).map(item => {
      productContainer.innerHTML += `
      <div class ="products">
        <ion-icon name="close-circle-outline"></ion-icon>
        <span>${item.order}</span>
        </div>
      <div hidden class ="price">$${item.price}</div>
      <div class ="quantity">
        <ion-icon class="decrease"
        name="arrow-back-circle"></ion-icon>
        <span>${item.quantity}</span>
        <ion-icon class="increase"
        name="arrow-forward-circle"></ion/icon>
      <div class="total">
        $${item.quantity * item.price}
      </div>
      `;
    });

    productContainer.innerHTML += `
      <div class="cartTotalContainer">
        <h4 class="cartTotalTitle">
          Cart total
        </h4>
        <h4 class="cartTotal">
          $${cartCost}.00
        </h4>
      </div>
      <form class="inputForm">
          <label for "name">Name</label>
          <input class="inputName" id="name" type="text"/>
          <label for "email">Email</email>
          <input class="inputEmail" id="email" type="email"/>
          <input class="formBtn" id="btn" type="submit" value="Submit Order"/>
          <button id="emptyCart" class ="emptyCart">Empty Cart?</button>
          </form>

    `;
  }
    onLoadCartNumbers();
    deleteButtons();
    manageQuantity();
}
displayCart();

let orders = [];
      const addOrder = (ev)=>{
            ev.preventDefault();  //to stop the form submitting
            var order = JSON.parse(localStorage.getItem("productsquantity"));
      const orderItems = {
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
				        order: []
                }
                if(typeof order.chocolatechip !== 'undefined') {
                    orderItems.order.push({"cookie":"Chocolate chip", "quantity": order.chocolatechip.quantity});
                  }
                if(typeof order.mangocranberry !== 'undefined') {
                    orderItems.order.push({"cookie":"Mango Cranberry", "quantity":order.mangocranberry.quantity});
                  }
                if(typeof order.plain !== 'undefined') {
                    orderItems.order.push({"cookie":"Plain", "quantity":order.plain.quantity});
                  }
      fetch('https://testingbakersear.herokuapp.com/api/submit_order' , {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(orderItems)
      }).then(function (response) {
        return response.text();
      }).then(function (text) {
        console.log(text);
      }).catch(function(error) {
        console.error(error);
      })
			window.localStorage.getItem('productsquantity');
            orders.push(orderItems);
            document.forms[0].reset(); // clears forms

            //displays order for testing
            console.warn('added' , {orders} );
            let pre = document.querySelector('#msg pre');

            //saving to localStorage
            localStorage.setItem('orderList', JSON.stringify(orders) );
        }
        document.addEventListener('DOMContentLoaded', ()=>{
        document.getElementById("btn").addEventListener('click', addOrder);
      });